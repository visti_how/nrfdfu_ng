% NRFDFU(1) nrfdfu_ng 1.0
% See AUTHORS file in source
% July 2023

# NAME
nrfdfu - NRF51/NRF52 (Secure) DFU client

# SYNOPSIS
**nrfdfu** **\-b** *address*  **\-p** *dfupackage.zip*
\[ **-c** *retry count* ]

# DESCRIPTION
The program **nrfdfu** is a a Linux utility for performing Bluetooth DFU
firmware upgrades for Nordic Semiconductor nRF52 and probably nRF51 using
a regular BT interface.

# OPTIONS
-b *address*
: The Bluetooth MAC address of the device to be upgraded (note it must
already be in DFU mode)

-p *dfupackage.zip*
: ZIP package with application binary, init data, and manifest

-c *retry count*
: Maximum number of retries (default 0)

-h
: Displays a help message.

-V
: Displays the software version.

# EXAMPLES
**nrfdfu \-b C0:AB:DC:F9:C3:5F \-p firmware.zip**
: Download application in firmware file to device in BLE mode


# EXIT VALUES
**0**
: Success

**1**
: Failure

# BUGS
Please report issues at <https://gitlab.com/visti_how/nrfdfu_ng/-/issues>

# COPYRIGHT
License GPLv2: GNU GPL version 2

